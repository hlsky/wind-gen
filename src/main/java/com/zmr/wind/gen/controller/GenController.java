package com.zmr.wind.gen.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zmr.wind.gen.entity.GenQo;
import com.zmr.wind.gen.entity.TableInfo;
import com.zmr.wind.gen.service.IGenService;
import com.zmr.wind.gen.util.Page;
import com.zmr.wind.gen.util.R;

import cn.hutool.core.convert.Convert;

/**
 * 代码生成 操作处理
 * 
 * @author ruoyi
 */
@Controller
@RequestMapping("/tool/gen")
public class GenController
{
    private String      prefix = "tool/gen";

    @Autowired
    private IGenService genService;

    @GetMapping()
    public String gen(Model model)
    {
        return prefix + "/gen";
    }

    @PostMapping("/list")
    @ResponseBody
    public Page list(TableInfo tableInfo, Page page)
    {
        PageHelper.startPage(page.getPage(), page.getPageSize(), "create_time desc");
        List<TableInfo> list = genService.selectTableList(tableInfo);
        page.setRows(list);
        page.setTotal(new PageInfo<TableInfo>(list).getTotal());
        return page;
    }

    /**
     * 生成代码
     */
    @RequestMapping("/genCode/{tableName}")
    @ResponseBody
    public R genCode(HttpServletResponse response, @PathVariable("tableName") String tableName, GenQo gq)
            throws IOException
    {
        byte[] data = genService.generatorCode(tableName, gq);
        if (null != data)
        {
            response.reset();
            response.setHeader("Content-Disposition", "attachment; filename=\"ruoyi.zip\"");
            response.addHeader("Content-Length", "" + data.length);
            response.setContentType("application/octet-stream; charset=UTF-8");
            IOUtils.write(data, response.getOutputStream());
        }
        return R.ok("代码已经生成");
    }

    /**
     * 批量生成代码
     */
    @RequestMapping("/batchGenCode")
    @ResponseBody
    public R batchGenCode(HttpServletResponse response, String tables, GenQo gq) throws IOException
    {
        byte[] data = genService.generatorCode(Convert.toStrArray(tables), gq);
        if (null != data)
        {
            response.reset();
            response.setHeader("Content-Disposition", "attachment; filename=\"ruoyi.zip\"");
            response.addHeader("Content-Length", "" + data.length);
            response.setContentType("application/octet-stream; charset=UTF-8");
            IOUtils.write(data, response.getOutputStream());
        }
        return R.ok("代码已经生成");
    }
}
