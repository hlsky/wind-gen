# ${tableComment}

${H2} ${tableComment}查询列表
**URL:** http://{server}/${moduleName}/${classname}/list

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
#foreach ($column in $columns)
$column.attrname|$column.attrType|$column.columnComment|否
#end

**请求示例:**
```
{
#foreach ($column in $columns)
#if($foreach.hasNext)
	"$column.attrname":$column.attrType,
#else
	"$column.attrname":$column.attrType
#end
#end
}

```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
rows|array|集合
total|number|记录总数


**返回示例:**
```
{
	"code":0,
	"msg":"success",
	"rows":{}
}
```

${H2} ${tableComment}根据编号查询
**URL:** http://{server}/${moduleName}/${classname}/get/{${primaryKey.attrname}}

**Type:** GET

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
${primaryKey.attrname}|${primaryKey.attrType}|主键|是

**请求示例:**
```
http://{server}/${moduleName}/${classname}/get/123
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容
data|object|对象


**返回示例:**
```
{
	"code":0,
	"msg":"success",
	"data":{
#foreach ($column in $columns)
#if($foreach.hasNext)
		"$column.attrname":$column.attrType,
#else
		"$column.attrname":$column.attrType
#end
#end
	}
}
```

${H2} ${tableComment}新增
**URL:** http://{server}/${moduleName}/${classname}/add

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
#foreach ($column in $columns)
$column.attrname|$column.attrType|$column.columnComment|否
#end

**请求示例:**
```
{
#foreach ($column in $columns)
#if($foreach.hasNext)
	"$column.attrname":$column.attrType,
#else
	"$column.attrname":$column.attrType
#end
#end
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```
{
	"code":0,
	"msg":"success"
}
```

${H2} ${tableComment}更新
**URL:** http://{server}/${moduleName}/${classname}/edit

**Type:** POST

**Content-Type:** application/json

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
#foreach ($column in $columns)
$column.attrname|$column.attrType|$column.columnComment|否
#end

**请求示例:**
```
{
#foreach ($column in $columns)
#if($foreach.hasNext)
	"$column.attrname":$column.attrType,
#else
	"$column.attrname":$column.attrType
#end
#end
}
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```
{
	"code":0,
	"msg":"success"
}
```

${H2} ${tableComment}删除
**URL:** http://{server}/${moduleName}/${classname}/remove

**Type:** POST

**Content-Type:** application/x-www-form-urlencoded

**Request-headers:**

Name | Type|Description
---|---|---
token|string|授权令牌


**Request-parameters:**

参数名 | 参数|描述|是否必须
---|---|---|---
ids|string|id字符串如"1,2,3"|是

**请求示例:**
```
url: 'http://{server}/${moduleName}/${classname}/remove',
method: 'post',
params: '1,2,3'
```
**响应数据:**

字段 | 类型|描述
---|---|---
code|int|返回编码，'0'表示成功
msg|string|消息内容


**返回示例:**
```
{
	"code":0,
	"msg":"success"
}
```