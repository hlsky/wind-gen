# wind-gen
提取[ruoyi](https://gitee.com/zhangmrit/RuoYi)代码生成模块，增加生成到项目选项和可视化配置

vm模版是自己使用的，比较简便，可以从原项目中获取并覆盖，亦可以修改源码，[模板地址](https://gitee.com/zhangmrit/RuoYi/tree/master/ruoyi-generator/src/main/resources/vm)

###### 效果如下
<table>
    <tr>
        <td><img src="https://gitee.com/zhangmrit/wind-gen/raw/master/img/show.png"/></td>
    </tr>
</table>